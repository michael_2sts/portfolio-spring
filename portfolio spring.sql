drop database if exists Portfolio;

CREATE DATABASE Portfolio;
USE Portfolio;

Create table inicio (
id int primary key AUTO_INCREMENT,
imagen varchar(255),
nombre varchar(255)
);

insert inicio (imagen, nombre) values ("/img/logo_azul.png", "Michael dos Santos");

Create table logos (
id int primary key AUTO_INCREMENT,
logo varchar(255)
);

insert logos (logo) values ("/img/ICONOS/HTML.png");
insert logos (logo) values ("/img/ICONOS/css3-logo-png-transparent.png");
insert logos (logo) values ("/img/ICONOS/PYTHON.png");
insert logos (logo) values ("/img/ICONOS/MYSQL.png");
insert logos (logo) values ("/img/ICONOS/JAVA1.png");


Create table header (
id int primary key AUTO_INCREMENT,
logo varchar(255),
texto varchar(255)
);

insert header (logo, texto) values ("/img/logo_azul.png", "INICIO");
insert header (texto) values ("SOBRE MÍ");
insert header (texto) values ("PROYECTOS");
insert header (texto) values ("CONTACTO");

Create table about (
id int primary key AUTO_INCREMENT,
imagen varchar(255),
texto varchar(1000)
);

insert about (texto, imagen) values ("¡Hola! Soy Michael dos Santos, un estudiante de programación apasionado por resolver problemas y
                crear soluciones innovadoras. Mi objetivo es ser técnico superior de desarrollo de aplicaciones y además ser
                trader. He participado en proyectos académicos y tengo proyectos en la seción proyectos. Creo en la importancia de la colaboración y la creatividad en el mundo de la programación. Estoy abierto a nuevas oportunidades y emocionado por seguir aprendiendo y creciendo en esta industria.", "/img/ICONOS/klipartz.com.png");


Create table proyecto (
id int primary key AUTO_INCREMENT,
imagen varchar(255),
texto varchar(255)
);

insert proyecto (texto, imagen) values ("Aquí hay algunos de los proyectos hechos por mí", "/img/ICONOS/proyectos/6.png");
insert proyecto (imagen) values ("/img/ICONOS/proyectos/5.png");
insert proyecto (imagen) values ("/img/ICONOS/proyectos/3.png");
insert proyecto (imagen) values ("/img/ICONOS/proyectos/1.png");
insert proyecto (imagen) values ("/img/ICONOS/proyectos/4.png");
insert proyecto (imagen) values ("/img/ICONOS/proyectos/2.png");


Create table footer (
id int primary key AUTO_INCREMENT,
logo varchar(255),
numero varchar(255),
email varchar(255)
);

insert footer (logo, numero, email) values ("/img/logo (1).png","+34 696696696","michaelantonio.s300149@cesurformacion.com");


select * 
from footer;