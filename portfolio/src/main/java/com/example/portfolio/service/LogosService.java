package com.example.portfolio.service;

import com.example.portfolio.model.Logos;
import com.example.portfolio.repository.LogosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


@SpringBootApplication
public class  LogosService{

    @Autowired
    private LogosRepository logosRepository;
    public List <Logos> seccion_logos() {
        List<Logos> a;
        a = logosRepository.findAll();
        return a;
    }

}
