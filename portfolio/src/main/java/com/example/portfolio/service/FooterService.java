package com.example.portfolio.service;

import com.example.portfolio.model.Footer;
import com.example.portfolio.model.Header;
import com.example.portfolio.repository.FooterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class FooterService {
    @Autowired
    private FooterRepository footerRepository;
    public List <Footer> seccion_footer() {
        List<Footer> a;
        a = footerRepository.findAll();
        return a;
    }
}
