package com.example.portfolio.service;

import com.example.portfolio.model.Portfolio;
import com.example.portfolio.repository.PortfolioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;


@SpringBootApplication
public class  PortfolioService{

    @Autowired
    private PortfolioRepository portfolioRepository;
    public List <Portfolio> inicio(){
        List<Portfolio> a;
        a = portfolioRepository.findAll();
        return a;
    }

}
