package com.example.portfolio.service;

import com.example.portfolio.model.About;
import com.example.portfolio.model.Header;
import com.example.portfolio.repository.AboutRepository;
import com.example.portfolio.repository.HeaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class AboutService {
    @Autowired
    private AboutRepository aboutRepository;
    public List <About> seccion_about() {
        List<About> a;
        a = aboutRepository.findAll();
        return a;
    }
}
