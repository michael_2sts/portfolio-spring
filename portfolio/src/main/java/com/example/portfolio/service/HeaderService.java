package com.example.portfolio.service;

import com.example.portfolio.model.Header;
import com.example.portfolio.repository.HeaderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class HeaderService {
    @Autowired
    private HeaderRepository headerRepository;
    public List <Header> seccion_header() {
        List<Header> a;
        a = headerRepository.findAll();
        return a;
    }
}
