package com.example.portfolio.service;

import com.example.portfolio.model.About;
import com.example.portfolio.model.Header;
import com.example.portfolio.model.Proyecto;
import com.example.portfolio.repository.AboutRepository;
import com.example.portfolio.repository.HeaderRepository;
import com.example.portfolio.repository.ProyectoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
public class ProyectoService {
    @Autowired
    private ProyectoRepository proyectoRepository;
    public List <Proyecto> seccion_proyecto() {
        List<Proyecto> a;
        a = proyectoRepository.findAll();
        return a;
    }
}
