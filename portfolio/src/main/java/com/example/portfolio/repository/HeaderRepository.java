package com.example.portfolio.repository;

import com.example.portfolio.model.Header;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HeaderRepository extends JpaRepository<Header, Long> {
}
