package com.example.portfolio.repository;

import com.example.portfolio.model.Logos;
import org.springframework.data.jpa.repository.JpaRepository;
public interface LogosRepository extends JpaRepository<Logos, Long>{
}

