package com.example.portfolio.controller;

import com.example.portfolio.PortfolioApplication;
import com.example.portfolio.model.*;
import com.example.portfolio.model.Portfolio;
import com.example.portfolio.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;


@Controller
public class BasicController {

    @Autowired
    private PortfolioService portfolioService;

    @Autowired
    private LogosService logosService;

    @Autowired
    private HeaderService headerService;

    @Autowired
    private AboutService aboutService;

    @Autowired
    private ProyectoService proyectoService;

    @Autowired
    private FooterService footerService;



    @RequestMapping("/")
    String inicio(Model model) {

        List<Portfolio> listaId=new ArrayList<>();
        List<Portfolio>aux= portfolioService.inicio();

            for(int i= 0;i<aux.size();i++){
                listaId.add(aux.get(i));
            }
            model.addAttribute("id",listaId);


        List<Logos> listaid1=new ArrayList<>();
        List<Logos>aux1= logosService.seccion_logos();

            for(int i= 0;i<aux1.size();i++){
                listaid1.add(aux1.get(i));
            }
         model.addAttribute("logos",listaid1);


        List<Header> listaid2=new ArrayList<>();
        List<Header>aux2= headerService.seccion_header();

        for(int i= 0;i<aux2.size();i++){
            listaid2.add(aux2.get(i));
        }
        model.addAttribute("header",listaid2);


        List<About> listaid3=new ArrayList<>();
        List<About>aux3= aboutService.seccion_about();

        for(int i= 0;i<aux3.size();i++){
            listaid3.add(aux3.get(i));
        }
        model.addAttribute("about",listaid3);


        List<Proyecto> listaid4=new ArrayList<>();
        List<Proyecto>aux4= proyectoService.seccion_proyecto();

        for(int i= 0;i<aux4.size();i++){
            listaid4.add(aux4.get(i));
        }
        model.addAttribute("proyecto",listaid4);


        List<Footer> listaid5=new ArrayList<>();
        List<Footer>aux5= footerService.seccion_footer();

        for(int i= 0;i<aux5.size();i++){
            listaid5.add(aux5.get(i));
        }
        model.addAttribute("footer",listaid5);


        return "practicav1";
    }




//    @PostMapping("/add")
//    String add1(@RequestParam String movie_name, @RequestParam long id, Model model) {
//        portfolio p = new portfolio();
//        p.setImagen(movie_name);
//        p.setId(id);
//        movieService.addMovie(p);
//        model.addAttribute("movies", p);
//        model.addAttribute("message", "La película '" + movie_name + "' ha sido añadida.");
//
//        return "actualizacion";
//    }
//
//    @RequestMapping("/delete")
//    String delete() {
//        return "deletemovie";
//    }

}